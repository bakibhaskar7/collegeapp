﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Data;

namespace CollegeApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesDataSetcontroller : ControllerBase
    {
        private readonly string connectionString = "Data Source=BHASKAR\\SQLEXPRESS;Initial Catalog=STUDENTDB;Integrated Security=true";

        [HttpGet]
        public ActionResult<List<MockEmployee>> GetEmployeeList()
        {
            try
            {
                DataSet dataSet = new DataSet();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "SELECT * FROM EMPLOYEESDEMO";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
                    {
                        con.Open();
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                        dataAdapter.Fill(dataSet, "EmployeesDEMO");
                    }
                }

                List<MockEmployee> employees = new List<MockEmployee>();
                foreach (DataRow row in dataSet.Tables["EmployeesDEMO"].Rows)
                {
                    MockEmployee employee = new MockEmployee();
                    employee.Empid = Convert.ToInt32(row["empid"]);
                    employee.Empname = row["empname"].ToString();
                    employee.Empage = Convert.ToInt32(row["empage"]);
                    employee.Empsalary = Convert.ToInt32(row["empsalary"]);
                    employees.Add(employee);
                }

                return Ok(employees);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost]
        [Route("saveEmployee")]
        public ActionResult<string> SaveEmployee(MockEmployee employee)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "INSERT INTO EMPLOYEESDEMO (empname, empage, empsalary) VALUES (@Empname, @Empage, @Empsalary)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
                    {
                        cmd.Parameters.AddWithValue("@Empname", employee.Empname);
                        cmd.Parameters.AddWithValue("@Empage", employee.Empage);
                        cmd.Parameters.AddWithValue("@Empsalary", employee.Empsalary);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                return Ok("Employee saved successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPut]
        public ActionResult<string> UpdateEmployee(MockEmployee updatedData)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sqlQuery = "UPDATE EMPLOYEESDEMO SET empname = @Empname, empage = @Empage, empsalary = @Empsalary WHERE empid = @Empid";
                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        command.Parameters.AddWithValue("@Empid", updatedData.Empid);
                        command.Parameters.AddWithValue("@Empname", updatedData.Empname);
                        command.Parameters.AddWithValue("@Empage", updatedData.Empage);
                        command.Parameters.AddWithValue("@Empsalary", updatedData.Empsalary);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                return Ok("Employee updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete]
        public ActionResult<string> DeleteEmployee(int empId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string deleteQuery = "DELETE FROM EMPLOYEESDEMO WHERE empid = @Empid";
                    using (SqlCommand command = new SqlCommand(deleteQuery, connection))
                    {
                        command.Parameters.AddWithValue("@Empid", empId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                return Ok("Employee deleted successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }

    public class MockEmployee
    {
        public int Empid { get; set; }
        public string Empname { get; set; }
        public int Empage { get; set; }
        public int Empsalary { get; set; }
    } 
}
