﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CollegeApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly string connectionString = "Data Source=BHASKAR\\SQLEXPRESS;Initial Catalog=STUDENTDB;Integrated Security=true";

        [HttpGet]
        public ActionResult<List<MockStudent>>GetStudentList()
        {
            List<MockStudent> students = new List<MockStudent>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlQuery = "SELECT * FROM STUDENTCONTEXT";

                using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        MockStudent student = new MockStudent();
                        student.StudentId = Convert.ToInt32(reader["STUDENTID"]);
                        student.StudentName = reader["STUDENTNAME"].ToString();
                        // Add other properties here if needed
                        students.Add(student);
                    }
                }
            }

            return Ok(students);
        }

        [HttpPost]
        [Route("savestudent")]
        public ActionResult<string> SaveStudent(MockStudent student)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "INSERT INTO STUDENTCONTEXT (STUDENTID, STUDENTNAME) VALUES (@StudentId, @StudentName)";

                    using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
                    {
                        cmd.Parameters.AddWithValue("@StudentId", student.StudentId);
                        cmd.Parameters.AddWithValue("@StudentName", student.StudentName);
                        // Add other parameters here if needed

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                return Ok("Student saved successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPut]
        [Route("updatestudent")]
        public ActionResult<string> UpdateStudent(MockStudent updatedData)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sqlQuery = "UPDATE STUDENTCONTEXT SET STUDENTNAME = @StudentName, STUDENTAGE = @StudentAge WHERE STUDENTID = @StudentId";

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        command.Parameters.AddWithValue("@StudentId", updatedData.StudentId);
                        command.Parameters.AddWithValue("@StudentName", updatedData.StudentName);
                        command.Parameters.AddWithValue("@StudentAge", updatedData.StudentAge);
                        // Add other parameters here if needed

                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                return Ok("Student updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete]
        [Route("deletestudent/{studentId}")]
        public ActionResult<string> DeleteStudent(int studentId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string deleteQuery = "DELETE FROM STUDENTCONTEXT WHERE STUDENTID = @StudentId";

                    using (SqlCommand command = new SqlCommand(deleteQuery, connection))
                    {
                        command.Parameters.AddWithValue("@StudentId", studentId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                return Ok("Student deleted successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }

    public class MockStudent
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int StudentAge { get; set; }
        // Add other properties here if needed
    }
}





