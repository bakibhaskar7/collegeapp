﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Data;

namespace CollegeApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeSPController : ControllerBase
    {
        private readonly string connectionString = "Data Source=BHASKAR\\SQLEXPRESS;Initial Catalog=STUDENTDB;Integrated Security=true";

        [HttpGet]
        public ActionResult<List<MockEmployee>> GetEmployeeLists()
        {
            List<MockEmployee> employees = new List<MockEmployee>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetEmployeeLists", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        MockEmployee employee = new MockEmployee();
                        cmd.CommandTimeout = 120;
                        employee.Empid = Convert.ToInt32(dr["Empid"]);
                        employee.Empname = dr["Empname"].ToString();
                        employee.Empage = Convert.ToInt32(dr["Empage"]);
                        employee.Empsalary = Convert.ToInt32(dr["Empsalary"]);
                        employees.Add(employee);
                    }
                }
            }
            return Ok(employees);
        }

        [HttpPost]
        [Route("saveEmployee")]
        public ActionResult<string> SaveEmployees(MockEmployee employee)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    // Check if employee with the same name already exists
                    using (SqlCommand checkCmd = new SqlCommand("SELECT COUNT(*) FROM Employees WHERE Empname = @Empname", con))
                    {
                        checkCmd.Parameters.AddWithValue("@Empname", employee.Empname);
                        con.Open();
                        int existingCount = (int)checkCmd.ExecuteScalar();
                        if (existingCount > 0)
                        {
                            return BadRequest("Employee with the same name already exists.");
                        }
                    }

                    // If no duplicate, proceed to insert
                    using (SqlCommand cmd = new SqlCommand("SaveEmployees", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Empname", employee.Empname);
                        cmd.Parameters.AddWithValue("@Empage", employee.Empage);
                        cmd.Parameters.AddWithValue("@Empsalary", employee.Empsalary);
                        cmd.ExecuteNonQuery();
                    }
                }
                return Ok("Employee saved successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }


        [HttpPut]
        public ActionResult<string> UpdateEmployees(MockEmployee updatedData)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("UpdateEmployees", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Empid", updatedData.Empid);
                        command.Parameters.AddWithValue("@Empname", updatedData.Empname);
                        command.Parameters.AddWithValue("@Empage", updatedData.Empage);
                        command.Parameters.AddWithValue("@Empsalary", updatedData.Empsalary);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
                return Ok("Employee updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete]
        public ActionResult<string> DeleteEmployees(int empId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("DeleteEmployees", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Empid", empId);

                        connection.Open();

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 0)
                        {
                            
                            return NotFound("Employee not found.");
                        }
                    }
                }
                return Ok("Employee deleted successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
        [HttpPost]
[Route("saveOrUpdateEmployee")]
        public ActionResult<string> SaveOrUpdateEmployee(MockEmployee employee)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Check if the employee exists
                    string query = "SELECT Empname, Empage, Empsalary FROM employeedemo WHERE empid = @empid";
                    using (SqlCommand checkCmd = new SqlCommand(query, con))
                    {
                        checkCmd.Parameters.AddWithValue("@empid", employee.Empid);
                        using (SqlDataReader reader = checkCmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                // Employee exists, check for changes before updating
                                reader.Read();
                                string existingName = reader.GetString(0);
                                int existingAge = reader.GetInt32(1);
                                decimal existingSalary = reader.GetDecimal(2);
                                reader.Close(); // Close the reader after reading data

                                if (employee.Empname != existingName || employee.Empage != existingAge || employee.Empsalary != existingSalary)
                                {
                                    // Changes detected, perform update
                                    using (SqlCommand updateCmd = new SqlCommand("UpdateEmployee", con))
                                    {
                                        updateCmd.CommandType = CommandType.StoredProcedure;
                                        updateCmd.Parameters.AddWithValue("@empid", employee.Empid);
                                        updateCmd.Parameters.AddWithValue("@empname", employee.Empname);
                                        updateCmd.Parameters.AddWithValue("@empage", employee.Empage);
                                        updateCmd.Parameters.AddWithValue("@empsalary", employee.Empsalary);
                                        updateCmd.ExecuteNonQuery();
                                    }
                                }
                            }
                            else
                            {
                                // Employee does not exist, perform insert
                                using (SqlCommand insertCmd = new SqlCommand("InsertEmployee", con))
                                {
                                    insertCmd.CommandType = CommandType.StoredProcedure;
                                    insertCmd.Parameters.AddWithValue("@Empname", employee.Empname);
                                    insertCmd.Parameters.AddWithValue("@Empage", employee.Empage);
                                    insertCmd.Parameters.AddWithValue("@Empsalary", employee.Empsalary);
                                    insertCmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
                return Ok("Employee saved or updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        public class MockEmployeeSP
        {
            public int Empid { get; set; }
            public string Empname { get; set; }
            public int Empage { get; set; }
            public int Empsalary { get; set; }
        }
    }
}