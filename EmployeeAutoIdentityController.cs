﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Diagnostics.Metrics;
using System.Security.Cryptography.X509Certificates;

namespace CollegeApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class EmployeeController : ControllerBase
    {
        private readonly string connectionString = "Data Source=BHASKAR\\SQLEXPRESS;Initial Catalog=STUDENTDB;Integrated Security=true";

        [HttpGet]
        public ActionResult<List<Mockemployee>> Getemployeelist()
        {
            List<Mockemployee> employees = new List<Mockemployee>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string Sqlquery = "select * from employeedemo";
                using (SqlCommand cmd = new SqlCommand(Sqlquery, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Mockemployee employee = new Mockemployee();
                        employee.Empid = Convert.ToInt32(dr["empid"]);
                        employee.Empname = dr["empname"].ToString();
                        employee.Empage = Convert.ToInt32(dr["empage"]);
                        employee.Empsalary = Convert.ToInt32(dr["empsalary"]);
                        employees.Add(employee);

                    }

                }

            }
            return Ok(employees);

        }

        [HttpPost]
        [Route("saveEmployee")]
        public ActionResult<string> SaveEmployee(Mockemployee employee)
        {

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sqlQuery = "INSERT INTO EMPLOYEEDEMO (empname,empage,empsalary) VALUES ( @Empname,@Empage,@Empsalary)";

                    using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
                    {
                      
                        cmd.Parameters.AddWithValue("@Empname", employee.Empname);
                        cmd.Parameters.AddWithValue("@Empage", employee.Empage);
                        cmd.Parameters.AddWithValue("@Empsalary", employee.Empsalary);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                return Ok("employee saved successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPut]

        public ActionResult<string> Updateemployee(Mockemployee updatedData)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sqlQuery = "UPDATE EMPLOYEEDEMO SET empname = @Empname,empage = @Empage,empsalary=@Empsalary WHERE empid=@Empid";

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        command.Parameters.AddWithValue("@Empid", updatedData.Empid);
                        command.Parameters.AddWithValue("@Empname", updatedData.Empname);
                        command.Parameters.AddWithValue("@Empage", updatedData.Empage);
                        command.Parameters.AddWithValue("@Empsalary", updatedData.Empsalary);
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();

                    }
                    

                    
                }

                return Ok("Employee updated successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpDelete]

        public ActionResult<string> Deleteemployee(int empId ,string empName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string deleteQuery = "DELETE FROM EMPLOYEEDEMO WHERE empid = 100 and empname = 'SEKHAR";

                    using (SqlCommand command = new SqlCommand(deleteQuery, connection))
                    {
                        command.Parameters.AddWithValue("@Empid", empId);
                        command.Parameters.AddWithValue("@Empname", empName);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                return Ok("Student deleted successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }

    public class Mockemployee
    {
        public int Empid { get; set; }
        public string Empname { get; set; }
        public int Empage { get; set; }
        public int Empsalary { get; set; }
    }
}











